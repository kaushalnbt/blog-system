<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Auth;
use App\Models\User;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;

class AuthController extends Controller
{
    protected $apiResponse;

    public function __construct(ApiResponse $apiResponse)
    {
        $this->apiResponse = $apiResponse;
    }

    public function register(Request $request)
    {
        try {
            $validator = Validator::make($request->all(), [
                'name' => 'required|string|max:255',
                'email' => 'required|string|email|max:255|unique:users',
                'password' => 'required|string|min:8',
            ]);

            if ($validator->fails()) {
                return $this->apiResponse->sendError(400, 'Validation Error', $validator->errors());
            }

            DB::beginTransaction();
            $user = User::create([
                'name' => $request->name,
                'email' => $request->email,
                'password' => Hash::make($request->password),
            ]);

            $token = $user->createToken('auth_token')->accessToken;
            DB::commit();

            return $this->apiResponse->sendResponse(200, 'User registered successfully', ['access_token' => $token, 'token_type' => 'Bearer']);
        } catch (\Exception $e) {
            DB::rollBack();
            return $this->apiResponse->sendError(500, $e->getMessage(), $e->getTraceAsString());
        }
    }

    public function login(Request $request)
    {
        try {
            if (!Auth::attempt($request->only('email', 'password'))) {
                return $this->apiResponse->sendError(401, 'Invalid login details');
            }

            $user = User::where('email', $request->email)->firstOrFail();

            $token = $user->createToken('auth_token')->accessToken;

            return $this->apiResponse->sendResponse(200, 'Login successful', ['access_token' => $token, 'token_type' => 'Bearer']);
        } catch (\Exception $e) {
            return $this->apiResponse->sendError(500, $e->getMessage(), $e->getTraceAsString());
        }
    }

    public function logout(Request $request)
    {
        try {
            $request->user()->currentAccessToken()->delete();

            return $this->apiResponse->sendResponse(200, 'Logged out successfully');
        } catch (\Exception $e) {
            return $this->apiResponse->sendError(500, $e->getMessage(), $e->getTraceAsString());
        }
    }

    public function user(Request $request)
    {
        return $this->apiResponse->sendResponse(200, 'User data retrieved successfully', Auth::user());
    }
}
