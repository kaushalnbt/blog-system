<?php

namespace App\Http\Controllers;

use App\Http\Controllers\ApiResponse;
use App\Http\Controllers\Controller;
use App\Models\Blog;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class CommentController extends Controller
{
    protected $apiResponse;

    public function __construct(ApiResponse $apiResponse)
    {
        $this->apiResponse = $apiResponse;
    }

    public function store(Request $request, Blog $blog)
    {
        $request->validate([
            'content' => 'required',
        ]);

        try {
            DB::beginTransaction();
            $comment = $blog->comments()->create($request->all());
            DB::commit();
            return $this->apiResponse->sendResponse(201, 'Comment added successfully', $comment);
        } catch (\Exception $e) {
            DB::rollBack();
            return $this->apiResponse->sendError(500, 'Failed to add comment');
        }
    }
}