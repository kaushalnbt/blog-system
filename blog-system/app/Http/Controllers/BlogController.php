<?php

namespace App\Http\Controllers;

use App\Http\Controllers\ApiResponse;
use App\Http\Controllers\Controller;
use App\Models\Blog;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class BlogController extends Controller
{
    protected $apiResponse;

    public function __construct(ApiResponse $apiResponse)
    {
        $this->apiResponse = $apiResponse;
    }

    public function index()
    {
        $blogs = Blog::paginate(10);
        return $this->apiResponse->sendResponse(200, 'Blogs retrieved successfully', $blogs);
    }

    public function show($id)
    {
        try {
            $blog = Blog::findOrFail($id);
            return $this->apiResponse->sendResponse(200, 'Blog retrieved successfully', $blog);
        } catch (\Exception $e) {
            return $this->apiResponse->sendError(404, 'Blog not found');
        }
    }

    public function store(Request $request)
    {
        $request->validate([
            'title' => 'required',
            'content' => 'required',
        ]);

        try {
            DB::beginTransaction();
            $blog = Blog::create($request->all());
            DB::commit();
            return $this->apiResponse->sendResponse(201, 'Blog created successfully', $blog);
        } catch (\Exception $e) {
            DB::rollBack();
            return $this->apiResponse->sendError(500, 'Failed to create blog');
        }
    }

    public function update(Request $request, $id)
    {
        $request->validate([
            'title' => 'required',
            'content' => 'required',
        ]);

        try {
            DB::beginTransaction();
            $blog = Blog::findOrFail($id);
            $blog->update($request->all());
            DB::commit();
            return $this->apiResponse->sendResponse(200, 'Blog updated successfully', $blog);
        } catch (\Exception $e) {
            DB::rollBack();
            return $this->apiResponse->sendError(500, 'Failed to update blog or blog not found');
        }
    }

    public function destroy($id)
    {
        try {
            DB::beginTransaction();
            $blog = Blog::findOrFail($id);
            $blog->delete();
            DB::commit();
            return $this->apiResponse->sendResponse(200, 'Blog deleted successfully');
        } catch (\Exception $e) {
            DB::rollBack();
            return $this->apiResponse->sendError(500, 'Failed to delete blog or blog not found');
        }
    }
}