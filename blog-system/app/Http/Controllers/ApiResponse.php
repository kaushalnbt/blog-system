<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class ApiResponse extends Controller
{
    public function sendResponse($code, $message, $data = null)
    {
        return response()->json([
            'status_code' => $code,
            'message' => $message,
            'data' => $data,
        ], $code);
    }

    public function sendError($code, $message, $data = null)
    {
        return response()->json([
            'status_code' => $code,
            'message' => $message,
            'errors' => $data,
        ], $code);
    }
}
